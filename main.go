package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "lang",
			Value: "english",
			Usage: "language for the greeting",
		},
	}
	app.Name = "snipper"
	app.Usage = "post and edit gitlab snips"
	app.Action = func(c *cli.Context) error {
		name := ""
		if c.NArg() > 0 {
			name = c.Args().Get(0)
		}
		if c.String("lang") == "spanish" {
			//fmt.Println("Hola", name)
		} else {
			//  	fmt.Println("Hello", name)
		}

		accessToken := "mUc2u1-QR2c6EX5m3fbC"
		var jsonStr = []byte("{}")
		client := &http.Client{}
		url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%v/snippets/1716748", name)
		req, _ := http.NewRequest("GET", url, bytes.NewBuffer(jsonStr))

		req.Header.Set("Private-Token", accessToken)

		resp, err := client.Do(req)
		if err != nil {
			log.Println("The client has not been created: %v", err.Error())
			return nil
		}
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Errorf("there is response error %v", err.Error())
			return nil
		}
		fmt.Printf("%v", string(body))

		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
